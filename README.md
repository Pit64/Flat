# Theme « Flat »

Converted for Recalbox 9.2+ by Pit64 - 4/16/24

Original repository: https://github.com/lilbud/es-theme-flat

## Original README

Theme 'Flat' v1.00 - 2/9/17 by lilbud
For use with EmulationStation (http://www.emulationstation.org/)

Thanks to Rookervik for assistance

*MAME Basic View*
![Image](http://i.imgur.com/bgJw4JP.png)
*SNES Detailed View*
![Image](http://i.imgur.com/qty1F7m.png)
*SNES System View*
![Image](http://i.imgur.com/UiwDW6q.png)

